@echo off

cmake -H. -B build -G Ninja ^
  -DANDROID_ABI=arm64-v8a ^
  -DANDROID_NDK=%ANDROID_SDK_ROOT%\ndk\23.1.7779620 ^
  -DCMAKE_BUILD_TYPE=Debug ^
  -DCMAKE_TOOLCHAIN_FILE=%ANDROID_SDK_ROOT%\ndk\23.1.7779620\build\cmake\android.toolchain.cmake ^
  -DANDROID_NATIVE_API_LEVEL=23 ^
  -DANDROID_TOOLCHAIN=clang ^
  -DANDROID_STL=c++_shared

cmake --build build

copy /Y build\addon.node ..\app\src\main\assets\addon.node
