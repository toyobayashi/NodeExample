#include <node_api.h>

NAPI_MODULE_INIT() {
  napi_value world;
  napi_create_string_utf8(env, "addon", NAPI_AUTO_LENGTH, &world);
  napi_set_named_property(env, exports, "hello", world);
  return exports;
}
